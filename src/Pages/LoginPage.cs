
using OpenQA.Selenium;
using src.Bases;


namespace src.Pages;

public class LoginPage: PageBases{  

    #region Mapeamento elementos
    By usuarioInputLocator = By.Id("username");
    By entrarButtonLocator = By.XPath("//input[contains(@class, 'btn-success') and @value='Entrar']");
   
    By senhaInputLocator = By.Id("password");

    By informacaoUsuarioSpanLocator = By.CssSelector("span.user-info");
    #endregion    

    #region Métodos   
    
    public void PreencherUsuario(string usuario)
    {
        SendKeys(usuarioInputLocator , usuario);
    }

    public void PreencherSenha(string senha)
    {
        SendKeys(senhaInputLocator , senha);
    }    

    public string ObterInformacaoUsuario()
    {   
        string texto = GetText(informacaoUsuarioSpanLocator);
        return texto;
    }   

    public void ClicarEmEntrar()
    {
        Click(entrarButtonLocator);
    }

    public void MaximizarJanelaJavaScript()
    {
        MaximizeWindowJavascript();
    }

    public void PreencherUsuariosJavaScript(string senha)
    {
        SendKeysEmJavascript(usuarioInputLocator , senha);
    } 
    
    public void PreencherSenhaJavaScript(string senha)
    {
        SendKeysEmJavascript(senhaInputLocator , senha);
    } 

    public void ClicarEmEntrarJavaScript()
    {
       ClickEmJavascript(entrarButtonLocator);
    } 

    #endregion
}
