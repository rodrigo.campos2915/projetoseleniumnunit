
using OpenQA.Selenium;
using src.Bases;


namespace src.Pages;

public class MenuPage: PageBases{  

    #region Mapeamento elementos  
    
    By gerenciarButtonLocator  = By.XPath("//span[@class='menu-text' and contains(text(), 'Gerenciar')]"); 
    By gerenciarProjetoAbaLocator  = By.XPath("//a[@href='/manage_proj_page.php' and text()='Gerenciar Projetos']");
    By criarNovoProjetoButtonLocator = By.XPath("//button[@type='submit' and contains(@class, 'btn btn-primary btn-white btn-round') and text()='Criar Novo Projeto']");
    By administradorIconeLocator = By.XPath("//*[@id='breadcrumbs']/ul/li/a");
    By gerenciarUsuariosLocator = By.XPath("//a[@href='/manage_user_page.php']");
    By gerenciarPerfisLocator = By.XPath("//a[@href='/manage_prof_menu_page.php']");
    By administradorSairSpanLocator = By.XPath("//span[@class='user-info']");
    By sairButtonLocator = By.XPath("//a[@href='/logout_page.php']");
    By preferenciasAbaLocator = By.XPath("//a[@href='/account_prefs_page.php']");
    By gerenciarColunasAbaLocator = By.XPath("//a[@href='/account_manage_columns_page.php']");
    By perfisAbaLocator = By.XPath("//a[@href='/account_prof_menu_page.php']");
    By tokenApisAbaLocator = By.XPath("//a[@href='/api_tokens_page.php']");
    #endregion    

    #region Métodos   

    public void ClicarEmGerenciar()
    {
        Click(gerenciarButtonLocator);
    }

    public void ClicarEmAbaGerenciarProjeto()
    {
        Click(gerenciarProjetoAbaLocator);
    }

    public void ClicarEmCriarNovoProjeto()
    {
        Click(criarNovoProjetoButtonLocator);
    }

    public void ClicarEmAdministrator()
    {
        Click(administradorIconeLocator);
    }

    public void ClicarEmGerenciarUsuarios()
    {
        Click(gerenciarUsuariosLocator);
    }

    public void ClicarEmGerenciarPefis()
    {
        Click(gerenciarPerfisLocator);
    }

    public void ClicarEmAdministradorSair()
    {
        Click(administradorSairSpanLocator);
    }

    public void ClicarEmIconeSair()
    {
        Click(sairButtonLocator);
    }
    
    public void ClicarEmAbaPreferencias()
    {
        Click(preferenciasAbaLocator);
    }

    public void ClicarEmAbaGerenciarColunas()
    {
        Click(gerenciarColunasAbaLocator);
    }

    public void ClicarEmAbaPerfis()
    {
        Click(perfisAbaLocator);
    }

    public void ClicarEmTokenApis()
    {
        Click(tokenApisAbaLocator);
    }





    #endregion
}
