using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using src.Bases;

namespace src.Pages
{
    public class RetonarTextoPage : PageBases
    {
        #region Mapeamento elementos
        By informacaoUsuarioSpanLocator = By.CssSelector("span.user-info");
        By informacaAlertaClassPLocator = By.XPath("//*[@class='alert alert-danger']/p");           
        By informacaConexaoBancoDadosServidorClassDangerLocator = By.CssSelector("td.danger");        
        By informacaoUsuarioSenhaAdminTr12Locator = By.XPath("//*[@id='main-container']/div[2]/div[2]/div[2]/div/div/table/tbody/tr[12]/td[2]");
        #endregion
      
        

        #region Métodos
        
        public string ObterInformacaoUsuarioLogin()
        {
            string texto = GetText(informacaoUsuarioSpanLocator);
            return texto;
        }

        public string ObterInformacaoAlertaLogin()
        {
            string texto = GetText(informacaAlertaClassPLocator);
            return texto;
        }

        public string ObterInformacaoConxaoBancoDadosIncorreto()
        {
            string texto = GetAttributeText(informacaConexaoBancoDadosServidorClassDangerLocator);
            return texto;
        }
        
        public string ObterInformacaoConxaoBancoDadosCredenciaisCorretas()
        {
            string texto = GetAttributeText(informacaoUsuarioSenhaAdminTr12Locator);
            return texto;
        }

        
        #endregion
    }
}