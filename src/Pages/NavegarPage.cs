using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using src.Bases;

namespace src.Pages
{
    public class NavegarPage : PageBases
    {
        
        #region Métodos
        public void Navegate(string url)
        {
            NavegatePage(url);            
        }

        public void MaximizarJanela()
        {
            WindowMaximize();
        }

        public void Refresh()
        {
            RefreshPage();
        }


        public string ReturnAbsolutePathUrl()
        {
            return AbsolutePath();            
        }

        
       
        #endregion
    }
}