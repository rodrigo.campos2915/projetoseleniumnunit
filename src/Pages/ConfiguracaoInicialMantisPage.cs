
using OpenQA.Selenium;
using src.Bases;


namespace src.Pages;

public class ConfiguracaoInicialMantisPage: PageBases{  

    #region Mapeamento elementos  
    
    By tipoBancoDadosLocator  = By.Id("db_type"); 

    By nomeServidorLocator  = By.XPath("//input[@name='hostname']");

    By nomeUsuarioLocator  = By.XPath("//input[@name='db_username']");

    By senhaLocator  = By.XPath("//input[@name='db_password']");

    By nomeUsuarioAdminLocator  = By.XPath("//input[@name='admin_username']");

    By senhaAdminLocator  = By.XPath("//input[@name='admin_password']");

    By instalarButtonLocator  = By.XPath("//input[@name='go' and @value='Install/Upgrade Database']");

    
    #endregion    

    #region Métodos    

    public void SelecionarOpcaoTipoBancoDados(string nomeTipoBancoDados)    {   
       
        SelectedOption(tipoBancoDadosLocator, nomeTipoBancoDados);
    }

    public void PreencherNomeServidor(string nomeServidor)
    {   
        ClearText(nomeServidorLocator);
        SendKeys(nomeServidorLocator , nomeServidor);
    }

    public void PreencherUsuario(string nomeUsuario)
    {   
        ClearText(nomeUsuarioLocator);
        SendKeys(nomeUsuarioLocator , nomeUsuario);
    }

    public void PreencherSenha(string senhaUsuario)
    {   
        ClearText(senhaLocator);
        SendKeys(senhaLocator, senhaUsuario);
    }

    public void PreencherUsuarioAdmin(string nomeUsuario)
    {   
        ClearText(nomeUsuarioAdminLocator);
        SendKeys(nomeUsuarioAdminLocator , nomeUsuario);
    }

    public void PreencherSenhaAdmin(string senhaUsuario)
    {   
        ClearText(senhaAdminLocator);
        SendKeys(senhaAdminLocator, senhaUsuario);
    }

    public void ClicarEmInstalar()
    {
        Click(instalarButtonLocator);
    }
    
    #endregion
}