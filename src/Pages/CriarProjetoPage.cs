
using OpenQA.Selenium;
using src.Bases;


namespace src.Pages;

public class CriarProjetoPage: PageBases{  

    #region Mapeamento elementos  
    
    By nomeProjetoInputLocator = By.Id("project-name");

    By estadoIdLocator = By.Id("project-status");
    
    By herdarCategoriaGlobaisClassLocator = By.XPath("//*[@class='lbl']");

    By viabilidadeIdLocator = By.Id("project-view-state");

    By descricaoIdLocator = By.Id("project-description");   

    By adicionarButtonLocator = By.XPath("//input[@class='btn btn-primary btn-white btn-round' and @value='Adicionar projeto']"); 
    

    #endregion    

    #region Métodos  

    public void PreencherNomeProjeto(string nomeProjeto)
    {
        SendKeys(nomeProjetoInputLocator , nomeProjeto);
    }

    public void SelecionarOpcaoEstado(string nomeOpcao)
    {
        SelectedOption(estadoIdLocator , nomeOpcao);
    } 

    public void SelecionarCategoriasGlobais(bool selecionar=true)
    {   
        if (!selecionar){
            Click(herdarCategoriaGlobaisClassLocator);
        }       
    }  

    public void SelecionarOpcaoViabilidade(string nomeOpcao)
    {
        SelectedOption(viabilidadeIdLocator , nomeOpcao);
    }

    public void PreencherDescricao(string? descricao=null)
    {
        if (descricao != null)
        {
            SendKeys(descricaoIdLocator, descricao);            
        }       
    } 

    public void ClicarEmEntrar()
    {
        Click(adicionarButtonLocator);
    }
    
    #endregion
}
