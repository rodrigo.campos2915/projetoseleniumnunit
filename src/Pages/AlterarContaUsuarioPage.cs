
using OpenQA.Selenium;
using src.Bases;


namespace src.Pages;

public class AlterarContaUsuarioPage: PageBases{  

    #region Mapeamento elementos    
    By senhaAtualInputLocator = By.Id("password-current");   
    By novaSenhaLocator = By.Id("password");
    By confirmarNovaSenhaLocator = By.Id("password-confirm");    
    By AtualizarUsuarioButtonLocator = By.XPath("//input[@type='submit' and @class='btn btn-primary btn-white btn-round' and @value='Atualizar Usuário']");
    
    #endregion    

    #region Métodos   
   

     public void PreencherSenhaAtual(string senhaAtual)
    {
        SendKeys(senhaAtualInputLocator, senhaAtual);
    }

    public void PreencherNovaSenha(string senhaNova)
    {
        SendKeys(novaSenhaLocator, senhaNova);
    }

    public void PreencherConfirmarNovaSenha(string confirmaNovaSenha)
    {
        SendKeys(confirmarNovaSenhaLocator, confirmaNovaSenha);
    }

    public void ClicarEmAtualizarUsuario()
    {
        Click(AtualizarUsuarioButtonLocator);
    }   
    #endregion
}
