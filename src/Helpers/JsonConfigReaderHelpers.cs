using Newtonsoft.Json.Linq;

namespace src.Helpers
{
    public class JsonConfigReader
    {   
        #region Objetos | Parametros
        private static JObject _jsonObject = new JObject();
        #endregion
        
        #region Metódo de leitura arquivo Json.
        public static string GetAppSetting(string key)
        {   
            string root_path = GeneralHelpers.GetProjectPath();         
            string json = File.ReadAllText(Path.Combine(root_path, "appsettings.json"));  
            _jsonObject = JObject.Parse(json);
            return _jsonObject?[key]?.ToString() ?? throw new KeyNotFoundException($"The key '{key}' was not found in the appsettings.json file.");
        }
        #endregion
    }
}

