

namespace src.Helpers
{
    public class DadosCriacaoProjetoMantis
    {
        public string nomeProjeto { get; set; } = "";
        public string opcaoEstado { get; set; } = "";
        public string opcaoViabilidade { get; set; } = "";
        public string? descricao { get; set; }
        public bool selecionarCategoriaGlobais { get; set; } = true;
    }    

    public class DadosConfiguracaoInicialMantis
    {
        public string tipoBancoDados { get; set; } = "";
        public string nomeServidor { get; set; } = "";
        public string nomeUsuario { get; set; } = "";
        public string senhaUsuario { get; set; } = "";
        public string nomeUsuarioAdmin { get; set; } = "";
        public string nomeSenhaAdmin { get; set; } = "";
    }
}
