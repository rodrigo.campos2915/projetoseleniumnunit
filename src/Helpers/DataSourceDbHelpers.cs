using System;
using System.Collections.Generic;
using MongoDB.Driver;
using src.Core;

namespace src.Helpers
{
    public class DataSourceDbHelpers
    {
        private readonly DbConnectionManager dbConnectionManager;
         public QueryFileExecutor queryFileExecutor { get; }

        public DataSourceDbHelpers(string dbUrl, string dbName, string dbUser, string dbPassword, int dbPort)
        {
            dbConnectionManager = new DbConnectionManager(dbUrl, dbName, dbUser, dbPassword, dbPort);
            var dbQueryExecutor = new DbQueryExecutor(dbConnectionManager.GetConnection());
            queryFileExecutor = new QueryFileExecutor(dbQueryExecutor);
        }

        public void DisconnectManager()
        {
            dbConnectionManager?.Dispose();
        }

        public static List<DadosCriacaoProjetoMantis> RetornarDadosEmMassa()
        {
            var dataSourceDb = new DataSourceDbHelpers(
                GlobalVariables.dbUrl,
                GlobalVariables.dbName,
                GlobalVariables.dbUser,
                GlobalVariables.dbPassword,
                GlobalVariables.dbPort
            );
            var resultado = dataSourceDb.queryFileExecutor.GetProjectData("DbQuerys\\ConsultaTabelaMassaDados.sql");
            dataSourceDb.DisconnectManager();
            return resultado;
        }
    }
}





