using OpenQA.Selenium;
using src.Helpers;



namespace src.Helpers
{
    public static class DriverHelper
    {
        #region Objetos | Parametros
        private static IWebDriver?_instance;
        #endregion
       
        #region intância driver
        public static IWebDriver INSTANCE
        {
            get
                {                
                    if (_instance == null)
                    {
                        throw new InvalidOperationException("A instância do driver não foi criada corretamente.");
                    }
                    return _instance;
                }
        }
        #endregion
        
        #region Configurar instância do driver
        public static void CreateInstance(string browser, bool isRemote, bool headless)
        {
            _instance = CreateDriverInstance(browser, isRemote, headless);
           
        }
        #endregion

        #region Criar instância driver
        private static IWebDriver CreateDriverInstance(string browser, bool isRemote, bool headless)
        {
            IWebDriver driver;

            if (isRemote)
            {
                driver = CreateRemoteDriver(browser, headless);
            }
            else
            {
                driver = CreateLocalDriver(browser, headless);
            }
            return driver;
        }
        #endregion

        #region Instânciar local
        private static IWebDriver CreateLocalDriver(string browser, bool headless)
        {
            IWebDriver? driver = null;


            switch (browser.ToLower())
            {
                case "chrome":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetLocalChromeHeadless();
                    }
                    else
                    {
                        driver = BrowsersHelpers.GetLocalChrome();
                    }
                    break;
                case "firefox":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetLocalFirefoxHeadless();
                    }
                    else
                    {
                        driver = BrowsersHelpers.GetLocalFirefox();
                    }
                    break;
                case "edge":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetLocalEdgeHeadless();

                    }
                    else
                    {
                        driver = BrowsersHelpers.GetLocalEdge();
                    }
                    break;
                // Adicione outros casos conforme necessário
                default:
                    throw new WebDriverException("Unsupported browser: " + browser);
            }

            return driver;
        }
        #endregion

        #region Instânciar remota
        private static IWebDriver CreateRemoteDriver(string browser, bool headless)
        {
            IWebDriver? driver = null;

            switch (browser.ToLower())
            {
                case "chrome":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetLocalChromeHeadless();

                    }
                    else
                    {
                        driver = BrowsersHelpers.GetRemoteChrome();

                    }
                    break;
                case "firefox":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetRemoteFirefoxHeadless();

                    }
                    else
                    {
                        driver = BrowsersHelpers.GetRemoteFirefox();
                    }
                    break;
                case "edge":
                    if (headless)
                    {
                        driver = BrowsersHelpers.GetRemoteEdgeHeadless();

                    }
                    else
                    {
                        driver = BrowsersHelpers.GetRemoteEdge();
                    }
                    break;
                // Adicione outros casos conforme necessário
                default:
                    throw new WebDriverException("Unsupported browser: " + browser);
            }

            return driver;
        }
        #endregion

        #region Encerrar instância 
        public static void QuitInstance()
        {
            if (_instance != null)
            {
                _instance.Quit();
                _instance = null;
            }
        }
        #endregion
    }
}