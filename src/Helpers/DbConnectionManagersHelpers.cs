using MySql.Data.MySqlClient;
using System;
using System.Threading.Tasks;

namespace src.Helpers
{
    public class DbConnectionManager : IDisposable
    {
        private MySqlConnection connection;
        private bool disposed = false;

        public DbConnectionManager(string server, string database, string username, string password, int port)
        {
            string connectionString = $"Server={server};Database={database};User ID={username};Password={password};Port={port}";
            connection = new MySqlConnection(connectionString);            
            try
            {
                connection.Open();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine($"MySqlException: {ex.Message}");              
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");                
            }
        }
        
        public async Task InitializeAsync(string server, string database, string username, string password, int port)
        {
            string connectionString = $"Server={server};Database={database};User ID={username};Password={password};Port={port}";
            connection = new MySqlConnection(connectionString);          
            try
            {
                await connection.OpenAsync();
            }
            catch (MySqlException ex)
            {
                Console.WriteLine($"MySqlException: {ex.Message}");                
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");                
            }
        }

        public MySqlConnection GetConnection()
        {
            return connection;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (connection != null)
                    {
                        connection.Close();
                        connection.Dispose();
                    }
                }

                disposed = true;
            }
        }

        ~DbConnectionManager()
        {
            Dispose(false);
        }
    }
}

