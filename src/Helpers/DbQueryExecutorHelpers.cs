using MySql.Data.MySqlClient;
using System.Data;

namespace src.Helpers
{
    public class DbQueryExecutor
    {
        private readonly MySqlConnection connection;

        public DbQueryExecutor(MySqlConnection connection)
        {
            this.connection = connection;
        }

        public DataTable ExecuteQuery(string query)
        {
            using (var command = new MySqlCommand(query, connection))
            {
                using (var adapter = new MySqlDataAdapter(command))
                {
                    var dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    return dataTable;
                }
            }
        }
    }
}
