using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using ExcelDataReader;
using src.Core;

namespace src.Helpers
{
    public static class DataDrivenHelpers
    {
        public static List<object[]> RetornarUsuarioSenha_XLSX(string nameFile)
        {
            var fileComplete = Path.Combine(GeneralHelpers.GetProjectPath(), nameFile);
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            var data = new List<object[]>();

            using (var stream = File.Open(fileComplete, FileMode.Open, FileAccess.Read))
            using (var reader = ExcelReaderFactory.CreateReader(stream))
            {
                var result = reader.AsDataSet();
                var dataTable = result.Tables[0];

                // Iniciar a partir da segunda linha (índice 1)
                for (int i = 1; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    var name = row[0]?.ToString() ?? string.Empty;
                    var password = row[1]?.ToString() ?? string.Empty;
                    data.Add(new object[] { name, password });
                }
            }

            return data;
        }
    }
}



