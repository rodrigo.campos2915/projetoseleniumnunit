using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using OpenQA.Selenium;
using System;
using System.IO;
using src.Core;
using System.Text.RegularExpressions;

namespace src.Helpers
{
    public static class ExtentReportHelpers
    {   
        #region Objetos | Parametros
        private static AventStack.ExtentReports.ExtentReports? extentReport;
        private static ExtentTest? test;
        private static readonly string reportName = $"{GlobalVariables.reportName}_{DateTime.Now:dd-MM-yyyy_HH-mm}";
        private static readonly string reportPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports", reportName);
        private static string methodName = string.Empty;
        #endregion

        #region Método de classe
        static ExtentReportHelpers()
        {
            Directory.CreateDirectory(reportPath);
        }

        public static void CreateReport()
        {
            if (extentReport == null)
            {
                var htmlReporter = new ExtentHtmlReporter(reportPath);
                extentReport = new AventStack.ExtentReports.ExtentReports();
                extentReport.AttachReporter(htmlReporter);
            }
        }

        public static void AddTest(string testName)
        {
            methodName = testName;
            test = extentReport?.CreateTest(testName) ?? throw new InvalidOperationException("Extent report is not initialized. Call CreateReport() first.");
        }

        public static void AddTestInfo(int methodLevel, string text)
        {
            if (test == null) throw new InvalidOperationException("Test is not initialized. Call AddTest() first.");

            var logText = $"{GeneralHelpers.GetMethodNameByLevel(methodLevel)} || {text}";
            if (GlobalVariables.screenshotStep == "true")
            {
                test.Log(Status.Pass, logText, GetScreenshotMedia());
            }
            else
            {
                test.Log(Status.Pass, logText);
            }
        }

        public static void AddTestResult(string status, string message, string stacktrace)
        {
            if (test == null) throw new InvalidOperationException("Test is not initialized. Call AddTest() first.");
            
            var logStatus = status switch
            {
                "Failed" => Status.Fail,
                "Inconclusive" => Status.Warning,
                "Skipped" => Status.Skip,               
                _ => Status.Pass
            };

            var logMessage = $"Test Result: {logStatus}<pre>Message: {message}</pre><pre>Stack Trace: {stacktrace}</pre>";
            test.Log(logStatus, logMessage, GetScreenshotMedia());
        }

        public static void GenerateReport()
        {
            extentReport?.Flush();
        }

        private static MediaEntityModelProvider GetScreenshotMedia()
        {
            string screenshotPath = GetScreenshotPath(reportPath);
            return MediaEntityBuilder.CreateScreenCaptureFromPath(screenshotPath.Replace(reportPath, ".")).Build();
        }

        private static string GetScreenshotPath(string path)
        {
            string methodNameWithoutParams = Regex.Replace(methodName, @"\(.*\)", "");
            string fileName = $"{methodNameWithoutParams}_{DateTime.Now:dd-MM-yyyy_HH-mm-ss}.png";
            string filePath = Path.Combine(path, fileName);
            
            var screenshot = ((ITakesScreenshot)DriverHelper.INSTANCE).GetScreenshot();
            screenshot.SaveAsFile(filePath);

            return filePath;
        }
        #endregion
    }
}


