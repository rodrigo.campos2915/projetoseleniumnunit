using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using MySql.Data.MySqlClient;

namespace src.Helpers
{
    public class QueryFileExecutor
    {
        private readonly DbQueryExecutor dbQueryExecutor;

        public QueryFileExecutor(DbQueryExecutor dbQueryExecutor)
        {
            this.dbQueryExecutor = dbQueryExecutor;
        }

        public DataTable ExecuteQueryFromFileWithParameters(string filePath, params (string Key, string Value)[] parameters)
        {
            string rootPath = GeneralHelpers.GetProjectPath();
            string path = Path.Combine(rootPath, filePath);
            string query = File.ReadAllText(path);

            foreach (var param in parameters)
            {
                query = query.Replace($"{{{param.Key}}}", param.Value);
            }

            return dbQueryExecutor.ExecuteQuery(query);
        }

        public DataTable ExecuteQueryFromFileWithoutParameters(string filePath)
        {
            string rootPath = GeneralHelpers.GetProjectPath();
            string path = Path.Combine(rootPath, filePath);
            string query = File.ReadAllText(path);
            return dbQueryExecutor.ExecuteQuery(query);
        }

        public List<DadosCriacaoProjetoMantis> GetProjectData(string filePath)
        {
            List<DadosCriacaoProjetoMantis> projetos = new List<DadosCriacaoProjetoMantis>();
            string rootPath = GeneralHelpers.GetProjectPath();
            string path = Path.Combine(rootPath, filePath);
            string query = File.ReadAllText(path);

            var dataTable = dbQueryExecutor.ExecuteQuery(query);
            if (dataTable == null)
            {
                throw new InvalidOperationException("Query execution returned null DataTable.");
            }

            using (var reader = dataTable.CreateDataReader())
            {
                if (reader == null)
                {
                    throw new InvalidOperationException("Failed to create DataTableReader from DataTable.");
                }

                while (reader.Read())
                {
                    projetos.Add(new DadosCriacaoProjetoMantis
                    {
                        nomeProjeto = reader["nomeProjeto"].ToString()!,
                        opcaoEstado = reader["opcaoEstado"].ToString()!,
                        opcaoViabilidade = reader["opcaoViabilidade"].ToString()!,
                        descricao = reader["descricaoProjeto"].ToString()                        
                    });
                }
            }

            return projetos;
        }
    }
}

