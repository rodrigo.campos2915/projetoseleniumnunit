using System.Runtime.CompilerServices;
using System.Diagnostics;


namespace src.Helpers
{
    public static class GeneralHelpers
    {   
        #region Métodos
        public static string GetProjectPath()
        {
            string baseDirectory = AppContext.BaseDirectory;
            string actualPath = baseDirectory.Substring(0, baseDirectory.LastIndexOf("bin", StringComparison.Ordinal));
            return actualPath;
        }
        
        public static string GetProjectBinDebugPath()
        {
            return Path.Combine(GetProjectPath(), "bin", "Debug", "net8.0");
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetMethodNameByLevel(int level)
        {
            StackTrace st = new StackTrace();
            StackFrame? sf = st.GetFrame(level);
            return sf?.GetMethod()?.Name ?? throw new InvalidOperationException("Could not get method name.");
        }
        #endregion
    }
}

