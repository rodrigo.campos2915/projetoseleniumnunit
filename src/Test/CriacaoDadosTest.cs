using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;

namespace src.Test
{
    [TestFixture, Order(3)]
    public class CriacaoDadosTest : TestsBase
    {
        #region Objetos
        private  DataSourceDbHelpers? dataSourceDb;     
        #endregion

        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup();          
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);           
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }
            dataSourceDb?.DisconnectManager();           
        }
        #endregion

        #region Testes
        [Test]
        [Order(1)]
        public void CriarTabelaDbMassaDadosCriarProjetosMantis()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros        
            dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\CriarTabelaMassaDados.sql");
            var resultadoVerificacao = dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\VerificarCriacaoTabelaMassaDados.sql");
            Assert.That(resultadoVerificacao, Is.Not.Null);
            #endregion
        }                  

        [Test]
        [Order(2)]
        public void InserirMassaDeDadosNaTabelaDbMassaDadosCriarProjetosMantis()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros
            dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys//InserirMassaDeDados.sql");
            var resultadoCriacao = dataSourceDb?.queryFileExecutor?.ExecuteQueryFromFileWithoutParameters("DbQuerys\\ConsultaTabelaMassaDados.sql");
            Assert.That(resultadoCriacao?.Rows.Count, Is.GreaterThan(0));
            #endregion
        }     

        #endregion

        #region Métodos Auxiliares
        private void ConfigurarConexaoDbMantis()
        {
            dataSourceDb = new DataSourceDbHelpers(GlobalVariables.dbUrl, GlobalVariables.dbName, GlobalVariables.dbUser, GlobalVariables.dbPassword, GlobalVariables.dbPort);
        }      
      
        #endregion
    }
}
