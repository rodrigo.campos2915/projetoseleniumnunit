using NUnit.Framework;
using NUnit.Framework.Interfaces;
using src.Basetests;
using src.Core;
using src.Flows;
using src.Helpers;
using src.Pages;
using System;

namespace src.Test
{
    [TestFixture, Order(7)]
    public class CriarProjetosEmMassaTest : TestsBase
    {
        private LoginFlows loginFlows;
        private NavegarPage navegarPage;
        private CriarProjetoPage criarProjetoPage;
        private MenuPage menuPage;

        [SetUp]
        public override void Setup()
        {
            base.Setup();
            var testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url);
            navegarPage.MaximizarJanela();           
            
            InicializarObjetos();
        }

        [TearDown]
        public void Dispose()
        {
            var status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            var stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            var message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }           
        }

        [TestCaseSource(typeof(DataSourceDbHelpers), nameof(DataSourceDbHelpers.RetornarDadosEmMassa))]
        [Order(1)]
        public void CriarNovoProjetoEmMassa(DadosCriacaoProjetoMantis dados)
        {
            var areaCriacaoNovoProjeto = "/manage_proj_create.php";

            loginFlows.EfetuarLogin("administrator", "2927");
            menuPage.ClicarEmGerenciar();
            menuPage.ClicarEmAbaGerenciarProjeto();
            menuPage.ClicarEmCriarNovoProjeto();
            criarProjetoPage.PreencherNomeProjeto(dados.nomeProjeto);
            criarProjetoPage.SelecionarOpcaoEstado(dados.opcaoEstado);
            criarProjetoPage.SelecionarCategoriasGlobais(dados.selecionarCategoriaGlobais);
            criarProjetoPage.SelecionarOpcaoViabilidade(dados.opcaoViabilidade);
            criarProjetoPage.PreencherDescricao(dados.descricao);
            criarProjetoPage.ClicarEmEntrar();

            Assert.That(navegarPage.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
        }        

        private void InicializarObjetos()
        {
            loginFlows = new LoginFlows();
            criarProjetoPage = new CriarProjetoPage();
            menuPage = new MenuPage();
        }
    }
}












