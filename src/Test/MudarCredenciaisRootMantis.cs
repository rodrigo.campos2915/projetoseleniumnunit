using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;

namespace src.Test
{
    [TestFixture, Order(2)]
    public class CadastrarUsuarioSenhaInicialTest : TestsBase
    {
        #region Objetos       
        private LoginPage? loginPage;
        private LoginFlows? loginPageFlows;
        private MenuFlows? menuFlows;
        private AlterarContaUsuarioFlows? alterarContaUsuarioFlows;
        private RetonarTextoPage? retornarTextoPage;
        private NavegarPage? navegarPage;  

        #endregion

        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup();
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url);
            navegarPage.MaximizarJanela();
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }            
        }
        #endregion       
        
        #region Testes
        [Test, Order(1)]
        public void RealizarLoginIncorreto()
        {
            RealizarLogin("administrator", "2927", false);
        }

        [Test, Order(2)]
        public void RealizarLoginComSucesso()
        {
            RealizarLogin("administrator", "root", true);
        }     

        [Test, Order(3)]
        public void RealizarLoginIncorreto_Flows()
        {
            RealizarLoginComFlows("administrator", "2927", false);
        }

        [Test, Order(4)]
        public void RealizarLoginComSucesso_Flows()
        {
            RealizarLoginComFlows("administrator", "root", true);
        }
        
        [Test, Order(5)]
        public void AcessarMinhaConta_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "root";
            string areaCriacaoNovoProjeto = "/account_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion

            #region Ações a serem testadas
            menuFlows?.NavegarParaAreaMinhaConta();           
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }  

        [Test, Order(6)]
        public void AlterarContaUsuario_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "root";
            string novasenha = "2927";
            string areaCriacaoNovoProjeto = "/account_update.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion

            #region Ações a serem testadas
            menuFlows?.NavegarParaAreaMinhaConta();  
            alterarContaUsuarioFlows?.CadastrarConta(senhaUsuario, novasenha, novasenha);        
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }  

        #endregion

        #region Métodos Auxiliares
        private void InicializarObjetos()
        {
            loginPage = new LoginPage(); 
            loginPageFlows = new LoginFlows(); 
            retornarTextoPage = new RetonarTextoPage();
            menuFlows = new MenuFlows();
            alterarContaUsuarioFlows = new AlterarContaUsuarioFlows();
                      
        }

        private void RealizarLogin(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Ações         
            loginPage?.PreencherUsuario(nomeUsuario);
            loginPage?.ClicarEmEntrar();
            loginPage?.PreencherSenha(senhaUsuario);
            loginPage?.ClicarEmEntrar();

            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }
        
        private void RealizarLoginComFlows(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Ações
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);

            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }
        
        #endregion
    }
}

