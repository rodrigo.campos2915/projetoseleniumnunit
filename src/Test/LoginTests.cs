using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;
using System.Collections.Generic;

namespace src.Test
{
    [TestFixture, Order(5)]
    public class LoginTest : TestsBase
    {
        #region Objetos      
        private LoginPage? loginPage;
        private LoginFlows? loginPageFlows;
        private RetonarTextoPage? retornarTextoPage;
        private NavegarPage? navegarPage;
        #endregion
        
        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup();
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url);
            navegarPage.MaximizarJanela();
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }
        }
        #endregion

        #region Testes

        [TestCaseSource(typeof(DataDrivenHelpers), nameof(DataDrivenHelpers.RetornarUsuarioSenha_XLSX), new object[] { "files\\listaExcelUsuarioSenhaIncorretosDataDriven.xlsx"})]
        public void RealizarLoginComUsuarioSenhaIncorreta_ComDataDriven(string nomeUsuario, string senhaUsuario)
        {
            RealizarLogin(nomeUsuario, senhaUsuario, false);
        }

        [TestCaseSource(typeof(DataDrivenHelpers), nameof(DataDrivenHelpers.RetornarUsuarioSenha_XLSX), new object[] { "files\\listaExcelUsuarioSenhaIncorretosDataDriven.xlsx"})]
        public void RealizarLoginComUsuarioSenhaIncorreta_ComDataDriven_Flows(string nomeUsuario, string senhaUsuario)
        {
            RealizarLoginComFlows(nomeUsuario, senhaUsuario, false);
        }

        [TestCaseSource(typeof(DataDrivenHelpers), nameof(DataDrivenHelpers.RetornarUsuarioSenha_XLSX), new object[] { "files\\listaExcelUsuarioSenhaCorretosDataDriven.xlsx"})]
        public void RealizarLoginComUsuarioSenhaCorreta_ComDataDriven(string nomeUsuario, string senhaUsuario)
        {
            RealizarLogin(nomeUsuario, senhaUsuario, true);
        }

        [TestCaseSource(typeof(DataDrivenHelpers), nameof(DataDrivenHelpers.RetornarUsuarioSenha_XLSX), new object[] { "files\\listaExcelUsuarioSenhaCorretosDataDriven.xlsx"})]
        public void RealizarLoginComUsuarioSenhaCorreta_ComDataDriven_Flows(string nomeUsuario, string senhaUsuario)
        {
            RealizarLoginComFlows(nomeUsuario, senhaUsuario, true);
        }

        [Test]
        public void RealizarLoginComSucesso()
        {
            RealizarLogin("administrator", "2927", true);
        }

        [Test]
        public void RealizarLoginComSucesso_Flows()
        {
            RealizarLoginComFlows("administrator", "2927", true);
        }

        [Test]
        public void RealizarLoginComSenhaIncorreta()
        {
            RealizarLogin("administrator", "wrong_password", false);
        }

        [Test]
        public void RealizarLoginComSenhaIncorreta_Flows()
        {
            RealizarLoginComFlows("administrator", "wrong_password", false);
        }

        [Test]
        public void RealizarLoginComSucessoEmJavaScript()
        {
            RealizarLoginJavaScript("administrator", "2927", true);
        }

        [Test]
        public void RealizarLoginComSucesso_Flows_JavaScript()
        {
            RealizarLoginComFlowsJavaScript("administrator", "2927", true);
        }

        [Test]
        public void RealizarLoginComSenhaIncorretaJavaScript()
        {
            RealizarLoginJavaScript("administrator", "wrong_password", false);
        }

        [Test]
        public void RealizarLoginComSenhaIncorreta_Flows_JavaScript()
        {
            RealizarLoginComFlowsJavaScript("administrator", "wrong_password", false);
        }

        #endregion

        #region Métodos Auxiliares

        private void InicializarObjetos()
        {
            loginPage = new LoginPage(); 
            loginPageFlows = new LoginFlows(); 
            retornarTextoPage = new RetonarTextoPage();          
        }

        private void RealizarLogin(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            #region Instanciar objetos
            InicializarObjetos();
            #endregion
            
            #region Ações
            loginPage?.PreencherUsuario(nomeUsuario);
            loginPage?.ClicarEmEntrar();
            loginPage?.PreencherSenha(senhaUsuario);
            loginPage?.ClicarEmEntrar();
            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }

        private void RealizarLoginComFlows(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            #region Instanciar objetos
            InicializarObjetos();
            #endregion
            
            #region Ações
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }

        private void RealizarLoginJavaScript(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            #region Instanciar objetos
            InicializarObjetos();
            #endregion
            
            #region Ações
            loginPage?.MaximizarJanelaJavaScript();
            loginPage?.PreencherUsuariosJavaScript(nomeUsuario);
            loginPage?.ClicarEmEntrarJavaScript();
            loginPage?.PreencherSenhaJavaScript(senhaUsuario);
            loginPage?.ClicarEmEntrarJavaScript();
            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }

        private void RealizarLoginComFlowsJavaScript(string nomeUsuario, string senhaUsuario, bool sucesso)
        {
            loginPageFlows = new LoginFlows();
            retornarTextoPage = new RetonarTextoPage();
            
            #region Ações
            loginPageFlows.EfetuarLoginEmJavaScript(nomeUsuario, senhaUsuario);
            if (sucesso)
            {
                Assert.That(retornarTextoPage?.ObterInformacaoUsuarioLogin(), Is.EqualTo("administrator"));
            }
            else
            {
                Assert.That(retornarTextoPage?.ObterInformacaoAlertaLogin(), Is.Not.EqualTo("administrator"));
            }
            #endregion
        }

        #endregion
    }
}


