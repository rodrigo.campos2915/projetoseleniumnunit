using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;

namespace src.Test
{
    [TestFixture, Order(6)]
    public class CriarProjetoUnitarioTest : TestsBase
    {
        #region Objetos        
        private LoginFlows? loginPageFlows;        
        private NavegarPage? navegarPage;
        private CriarProjetoPage? criarProjetoPage;
        private CriarProjetoFlows? criarProjeroFlows;
        private DadosCriacaoProjetoMantis? dadosCriacaoProjetoMantis;
        private MenuPage? menuPages;
        private MenuFlows? menuFlows;
        private  DataSourceDbHelpers? dataSourceDb;    

        #endregion

        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup();          
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url);
            navegarPage.MaximizarJanela();
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }
            dataSourceDb?.DisconnectManager();

        }
        #endregion

        #region Testes
        [Test]
        [Order(1)]
        public void CriarNovoProjeto_EstadoDesenvolvimento_ViabalidadePublico_CategoriasGlobaisTrue()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string nomeProjeto = "ProjetoTeste0001";
            string opcaoEstado = "desenvolvimento";
            string opcaoViabilidade = "público";
            string descricaoProjeto = "Projeto teste";
            string areaCriacaoNovoProjeto = "/manage_proj_create.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion

            #region Ações a serem testadas
            // Incluir menu
            menuPages?.ClicarEmGerenciar();
            menuPages?.ClicarEmAbaGerenciarProjeto();
            menuPages?.ClicarEmCriarNovoProjeto();
            criarProjetoPage?.PreencherNomeProjeto(nomeProjeto);
            criarProjetoPage?.SelecionarOpcaoEstado(opcaoEstado);
            criarProjetoPage?.SelecionarCategoriasGlobais();
            criarProjetoPage?.SelecionarOpcaoViabilidade(opcaoViabilidade);
            criarProjetoPage?.PreencherDescricao(descricaoProjeto);
            criarProjetoPage?.ClicarEmEntrar();
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]
        [Order(2)]
        public void NovoProjetoCriadoDbMantisProjectTable_01_RegistroCorreto()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros
            var resultadoCriacao = dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\ConsultaCricaoProjeto.sql");
            Assert.That(resultadoCriacao, Is.Not.Null);
            Assert.That(resultadoCriacao.Rows.Count, Is.EqualTo(1));            
            #endregion
        }
        
        [Test]
        [Order(3)]
        public void CriarNovoProjeto_EstadoRelease_ViabalidadePublico_CategoriasGlobaisTrue()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string nomeProjeto = "ProjetoTeste0002";
            string opcaoEstado = "release";
            string opcaoViabilidade = "público";
            string descricaoProjeto = "Projeto teste";
            string areaCriacaoNovoProjeto = "/manage_proj_create.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion

            #region Ações a serem testadas
            menuPages?.ClicarEmGerenciar();
            menuPages?.ClicarEmAbaGerenciarProjeto();
            menuPages?.ClicarEmCriarNovoProjeto();
            criarProjetoPage?.PreencherNomeProjeto(nomeProjeto);
            criarProjetoPage?.SelecionarOpcaoEstado(opcaoEstado);
            criarProjetoPage?.SelecionarCategoriasGlobais();
            criarProjetoPage?.SelecionarOpcaoViabilidade(opcaoViabilidade);
            criarProjetoPage?.PreencherDescricao(descricaoProjeto);
            criarProjetoPage?.ClicarEmEntrar();
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]
        [Order(4)]
        public void NovoProjetoCriadoDbMantisProjectTable_02_RegistroIncorreto()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros
            var resultadoCriacao = dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\ConsultaCricaoProjeto.sql");
            Assert.That(resultadoCriacao, Is.Not.Null);
            Assert.That(resultadoCriacao.Rows.Count, Is.EqualTo(2));

            // Deletar dados tabela 
            dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\Deletar2Projetos.sql");
            #endregion
        }

        [Test]
        [Order(5)]
        public void CriarNovoProjeto_EstadoDesenvolvimento_ViabalidadePublico_CategoriasGlobaisTrue_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            dadosCriacaoProjetoMantis = new DadosCriacaoProjetoMantis
            {
                nomeProjeto = "ProjetoTeste0004",
                opcaoEstado = "desenvolvimento",
                opcaoViabilidade = "público",
                descricao = "Projeto teste"
            };
            string areaCriacaoNovoProjeto = "/manage_proj_create.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);            
            #endregion

            #region Ações a serem testadas
            menuFlows?.NavegarParaAreaCriacaoProjeto();
            criarProjeroFlows?.CriarProjeto(dadosCriacaoProjetoMantis);
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]
        [Order(6)]
        public void NovoProjetoCriadoDbMantisProjectTable_01_RegistroCorreto_Flows()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros
            var resultadoCriacao = dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\ConsultaCricaoProjeto.sql");
            Assert.That(resultadoCriacao, Is.Not.Null);
            Assert.That(resultadoCriacao.Rows.Count, Is.EqualTo(1));
            #endregion
        }
        
        [Test]
        [Order(7)]
        public void CriarNovoProjeto_EstadoRelease_ViabalidadePublico_CategoriasGlobaisTrue_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            dadosCriacaoProjetoMantis = new DadosCriacaoProjetoMantis
            {
                nomeProjeto = "ProjetoTeste0003",
                opcaoEstado = "release",
                opcaoViabilidade = "público",
                descricao = "Projeto teste"
            };
            string areaCriacaoNovoProjeto = "/manage_proj_create.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);            
            #endregion

            #region Ações a serem testadas
            menuFlows?.NavegarParaAreaCriacaoProjeto();
            criarProjeroFlows?.CriarProjeto(dadosCriacaoProjetoMantis);
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]
        [Order(8)]
        public void NovoProjetoCriadoDbMantisProjectTable_02_RegistroIncorreto_Flows()
        {
            #region Intânciar objetos
            ConfigurarConexaoDbMantis();
            #endregion

            #region Ações conferir registros
            var resultadoCriacao = dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\ConsultaCricaoProjeto.sql");
            Assert.That(resultadoCriacao, Is.Not.Null);
            Assert.That(resultadoCriacao.Rows.Count, Is.EqualTo(2));

            // Deletar dados tabela 
            dataSourceDb?.queryFileExecutor.ExecuteQueryFromFileWithoutParameters("DbQuerys\\Deletar2Projetos.sql");
            #endregion
        }

        #endregion

        #region Métodos Auxiliares
        private void ConfigurarConexaoDbMantis()
        {
            dataSourceDb = new DataSourceDbHelpers(GlobalVariables.dbUrl, GlobalVariables.dbName, GlobalVariables.dbUser, GlobalVariables.dbPassword, GlobalVariables.dbPort);
        }
       
        private void InicializarObjetos()
        {           
            loginPageFlows = new LoginFlows();            
            criarProjetoPage = new CriarProjetoPage();
            criarProjeroFlows = new CriarProjetoFlows();     
            menuPages =  new MenuPage();
            menuFlows =  new MenuFlows();             
            
        }
        #endregion
    }
}
