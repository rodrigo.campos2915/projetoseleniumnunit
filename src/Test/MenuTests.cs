using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;

namespace src.Test
{
    [TestFixture, Order(4)]
    public class MenuTest : TestsBase
    {
        #region Objetos       
        private LoginFlows? loginPageFlows;
        private NavegarPage? navegarPage;
        private MenuPage? menuPage;
        private MenuFlows? menuFlows;
        #endregion

        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup();
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url);
            navegarPage.MaximizarJanela();
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }            
        }
        #endregion

        #region Testes
                
        [Test]     
        public void AcessarAreaCriarNovoProjeto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/manage_proj_create_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion
            
            #region Ações a serem testadas           
            menuFlows?.NavegarParaAreaCriacaoProjeto();           
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]     
        public void AcessarAreaMinhaConta_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/account_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion
            
            #region Ações a serem testadas            
            menuFlows?.NavegarParaAreaMinhaConta();           
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]     
        public void AcessarAreaPreferencias_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/account_prefs_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas      
            menuFlows?.NavegarParaAbaPreferencias();        
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]     
        public void AcessarAreaGerenciarColunas_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/account_manage_columns_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas                        
            menuFlows?.NavegarParaAbaGerenciarColunas();
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]     
        public void AcessarAreaPerfis_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/account_prof_menu_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas      
            menuFlows?.NavegarParaAbaPerfis();                   
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]     
        public void AcessarAreaTokensApi_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaCriacaoNovoProjeto = "/api_tokens_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas            
            menuFlows?.NavegarParaAbaTokenApis();            
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaCriacaoNovoProjeto));
            #endregion
        }

        [Test]
        public void VerificarAcessoAreaGerenciarUsuarios_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaGerenciarUsuarios = "/manage_user_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas       
            menuFlows?.NavegarParaAreaGerenciarUsuarios();
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaGerenciarUsuarios));
            #endregion
        }

        [Test]
        public void VerificarAcessoAreaGerenciarPerfis_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string areaGerenciarPerfis = "/manage_prof_menu_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas    
            menuFlows?.NavegarParaAreaGerenciarPerfis();      
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(areaGerenciarPerfis));
            #endregion
        }

        [Test]
        public void VerificarLogout_Flows()
        {
            #region Intânciar objetos
            InicializarObjetos();
            #endregion
            
            #region Parametros
            string nomeUsuario = "administrator";
            string senhaUsuario = "2927";
            string paginaLogin = "/login_page.php";
            #endregion

            #region Logar sistema
            loginPageFlows?.EfetuarLogin(nomeUsuario, senhaUsuario);
            #endregion            
            
            #region Ações a serem testadas          
            menuFlows?.SairDoSistema();        
            Assert.That(navegarPage?.ReturnAbsolutePathUrl(), Is.EqualTo(paginaLogin));
            #endregion
        }

       
        #endregion

        #region Métodos Auxiliares
        private void InicializarObjetos()
        {
            loginPageFlows = new LoginFlows();           
            menuPage = new MenuPage();
            menuFlows = new MenuFlows();
        }
        #endregion
    }
}


