using src.Helpers;
using src.Pages;
using src.Basetests;
using src.Core;
using src.Flows;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using System;

namespace src.Test
{
    [TestFixture, Order(1)]
    public class ConfiguracaoInicialTest : TestsBase
    {
        #region Objetos      
        private NavegarPage? navegarPage; 
        private ConfiguracaoInicialMantisPage? configuracaoInicialMantisPage;
        private ConfiguracaoInicialMantisFlows? configuracaoInicialMantisFlows; 
        private RetonarTextoPage? retornarTextoPage;  
        private DadosConfiguracaoInicialMantis? dadosConfiguracaoInicialMantis;
        #endregion

        #region Configurações
        [SetUp]
        public override void Setup()
        {   
            base.Setup(); // Chama o Setup da classe base
            string testName = TestContext.CurrentContext.Test.Name ?? "UnknownTestName";
            ExtentReportHelpers.AddTest(testName);
            DriverHelper.CreateInstance(GlobalVariables.browser, GlobalVariables.isRemote, GlobalVariables.headless);
            navegarPage = new NavegarPage();
            navegarPage.Navegate(GlobalVariables.url + "/admin/install.php");
            navegarPage.MaximizarJanela();
            navegarPage?.Refresh();
        }
        #endregion

        #region Descarte
        [TearDown]
        public void Dispose()
        {
            string status = TestContext.CurrentContext.Result.Outcome.Status == TestStatus.Failed ? "Failed" : "Passed";
            string stacktrace = TestContext.CurrentContext.Result.StackTrace ?? string.Empty;
            string message = TestContext.CurrentContext.Result.Message ?? string.Empty;

            ExtentReportHelpers.AddTestResult(status, stacktrace, message);

            if (GlobalVariables.driverQuit)
            {
                DriverHelper.QuitInstance();
            }            
        }
        #endregion

        #region Testes
        [Test, Order(1)]     
        public void CriarConfiguracaoInicialNomeServidorIncorreto()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string tipoBancoDados = "MySQL Improved";
            string nomeServidor = "ma";
            string nomeUsuario = "mmmm";
            string senhaUsuario = "mmmm";
            string nomeUsuarioAdmin = "rrrr";
            string nomeSenhaAdmin = "rrrr";
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisPage?.SelecionarOpcaoTipoBancoDados(tipoBancoDados);
            configuracaoInicialMantisPage?.PreencherNomeServidor(nomeServidor);
            configuracaoInicialMantisPage?.PreencherUsuario(nomeUsuario);
            configuracaoInicialMantisPage?.PreencherSenha(senhaUsuario);
            configuracaoInicialMantisPage?.PreencherUsuarioAdmin(nomeUsuarioAdmin);
            configuracaoInicialMantisPage?.PreencherSenhaAdmin(nomeSenhaAdmin);            
            configuracaoInicialMantisPage?.ClicarEmInstalar();
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));      
            #endregion  
        }

        [Test, Order(2)]     
        public void CriarConfiguracaoInicialNomeUsuarioIncorreto()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string tipoBancoDados = "MySQL Improved";
            string nomeServidor = "mantis-db-1";
            string nomeUsuario = "mmmmm";
            string senhaUsuario = "mmmmm";
            string nomeUsuarioAdmin = "rrrr";
            string nomeSenhaAdmin = "rrrr";
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisPage?.SelecionarOpcaoTipoBancoDados(tipoBancoDados);
            configuracaoInicialMantisPage?.PreencherNomeServidor(nomeServidor);
            configuracaoInicialMantisPage?.PreencherUsuario(nomeUsuario);
            configuracaoInicialMantisPage?.PreencherSenha(senhaUsuario);
            configuracaoInicialMantisPage?.PreencherUsuarioAdmin(nomeUsuarioAdmin);
            configuracaoInicialMantisPage?.PreencherSenhaAdmin(nomeSenhaAdmin);            
            configuracaoInicialMantisPage?.ClicarEmInstalar();
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));      
            #endregion  
        }

        [Test, Order(3)]     
        public void CriarConfiguracaoInicialSenhaUsuarioIncorreto()
        {   
            #region Intânciar objetos
            InicializarObjetos();           
            #endregion

            #region Parametros
            string tipoBancoDados = "MySQL Improved";
            string nomeServidor = "mantis-db-1";
            string nomeUsuario = "mantisbt";
            string senhaUsuario = "mmmm";
            string nomeUsuarioAdmin = "rrrr";
            string nomeSenhaAdmin = "rrrr";
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisPage?.SelecionarOpcaoTipoBancoDados(tipoBancoDados);
            configuracaoInicialMantisPage?.PreencherNomeServidor(nomeServidor);
            configuracaoInicialMantisPage?.PreencherUsuario(nomeUsuario);
            configuracaoInicialMantisPage?.PreencherSenha(senhaUsuario);
            configuracaoInicialMantisPage?.PreencherUsuarioAdmin(nomeUsuarioAdmin);
            configuracaoInicialMantisPage?.PreencherSenhaAdmin(nomeSenhaAdmin);            
            configuracaoInicialMantisPage?.ClicarEmInstalar();
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));      
            #endregion  
        }

        [Test, Order(4)]     
        public void CriarConfiguracaoInicialnomeUsuarioAdminIncorreto()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string tipoBancoDados = "MySQL Improved";
            string nomeServidor = "mantis-db-1";
            string nomeUsuario = "mantisbt";
            string senhaUsuario = "mmmm";
            string nomeUsuarioAdmin = "rrrr";
            string nomeSenhaAdmin = "rrrr";
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisPage?.SelecionarOpcaoTipoBancoDados(tipoBancoDados);
            configuracaoInicialMantisPage?.PreencherNomeServidor(nomeServidor);
            configuracaoInicialMantisPage?.PreencherUsuario(nomeUsuario);
            configuracaoInicialMantisPage?.PreencherSenha(senhaUsuario);
            configuracaoInicialMantisPage?.PreencherUsuarioAdmin(nomeUsuarioAdmin);
            configuracaoInicialMantisPage?.PreencherSenhaAdmin(nomeSenhaAdmin);            
            configuracaoInicialMantisPage?.ClicarEmInstalar();
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));      
            #endregion  
        }
        
        [Test, Order(5)]     
        public void CriarConfiguracaoInicialnomeSenhaAdminIncorreto()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros
            string tipoBancoDados = "MySQL Improved";
            string nomeServidor = "mantis-db-1";
            string nomeUsuario = "mantisbt";
            string senhaUsuario = "mantisbt";
            string nomeUsuarioAdmin = "root";
            string nomeSenhaAdmin = "rrrr";
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisPage?.SelecionarOpcaoTipoBancoDados(tipoBancoDados);
            configuracaoInicialMantisPage?.PreencherNomeServidor(nomeServidor);
            configuracaoInicialMantisPage?.PreencherUsuario(nomeUsuario);
            configuracaoInicialMantisPage?.PreencherSenha(senhaUsuario);
            configuracaoInicialMantisPage?.PreencherUsuarioAdmin(nomeUsuarioAdmin);
            configuracaoInicialMantisPage?.PreencherSenhaAdmin(nomeSenhaAdmin);            
            configuracaoInicialMantisPage?.ClicarEmInstalar();
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));      
            #endregion  
        }

        [Test, Order(6)]     
        public void CriarConfiguracaoInicialNomeServidorIncorreto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros            
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "ma",
                nomeUsuario = "mmmm",
                senhaUsuario = "mmmm",
                nomeUsuarioAdmin = "rrrr",
                nomeSenhaAdmin = "rrrr"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));        
            #endregion
        }

        [Test, Order(7)]     
        public void CriarConfiguracaoInicialNomeUsuarioIncorreto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros            
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "mantis-db-1",
                nomeUsuario = "mmmm",
                senhaUsuario = "mmmm",
                nomeUsuarioAdmin = "rrrr",
                nomeSenhaAdmin = "rrrr"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));        
            #endregion
        }

        [Test, Order(8)]     
        public void CriarConfiguracaoInicialSenhaUsuarioIncorreto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros            
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "mantis-db-1",
                nomeUsuario = "mantisbt",
                senhaUsuario = "mmmm",
                nomeUsuarioAdmin = "rrrr",
                nomeSenhaAdmin = "rrrr"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));        
            #endregion
        }

        [Test, Order(9)]     
        public void CriarConfiguracaoInicialnomeUsuarioAdminIncorreto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros            
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "mantis-db-1",
                nomeUsuario = "mantisbt",
                senhaUsuario = "mantisbt",
                nomeUsuarioAdmin = "root",
                nomeSenhaAdmin = "rrrr"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));        
            #endregion
        }

        [Test, Order(10)]     
        public void CriarConfiguracaoInicialnomeSenhaAdminIncorreto_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            #endregion

            #region Parametros            
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "mantis-db-1",
                nomeUsuario = "mantisbt",
                senhaUsuario = "mantisbt",
                nomeUsuarioAdmin = "root",
                nomeSenhaAdmin = "rrrr"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosIncorreto();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("BAD"));        
            #endregion
        }

        [Test, Order(11)]     
        public void CriarConfiguracaoInicialCorreta_Flows()
        {   
            #region Intânciar objetos
            InicializarObjetos();
            navegarPage?.Navegate(GlobalVariables.url);
            #endregion

            #region Parametros
            dadosConfiguracaoInicialMantis = new DadosConfiguracaoInicialMantis
            {
                tipoBancoDados = "MySQL Improved",
                nomeServidor = "mantis-db-1",
                nomeUsuario = "mantisbt",
                senhaUsuario = "mantisbt",
                nomeUsuarioAdmin = "root",
                nomeSenhaAdmin = "root"
            };
            #endregion

            #region Ações a serem testadas
            configuracaoInicialMantisFlows?.FazerConfiguracaoInicial(dadosConfiguracaoInicialMantis);
            string? texto = retornarTextoPage?.ObterInformacaoConxaoBancoDadosCredenciaisCorretas();
            Assert.NotNull(texto, "Texto não pode ser nulo");
            Assert.That(texto.Split("<br>")[0], Is.EqualTo("GOOD"));      
            #endregion  
        }
        #endregion

        #region Métodos Auxiliares
        private void InicializarObjetos()
        {           
            configuracaoInicialMantisPage = new ConfiguracaoInicialMantisPage();
            configuracaoInicialMantisFlows = new ConfiguracaoInicialMantisFlows();           
            retornarTextoPage = new RetonarTextoPage();  
        }
        #endregion
    }
}

