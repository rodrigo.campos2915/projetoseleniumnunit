using System;
using System.IO;
using src.Helpers;

namespace src.Core
{
    public static class GlobalVariables
    {
        #region Parametros para configuração webdriver global
        public static readonly string browser;
        public static readonly bool isRemote;
        public static readonly bool headless;
        public static readonly string url;
        public static readonly string defaultTimeOut;
        public static readonly string seleniumHub;
        public static readonly bool driverQuit;
        #endregion

        #region Parametros para configuração repports global
        public static readonly string reportName;
        public static readonly string screenshotStep;
        #endregion
        
        #region Parametros para configuração banco de dados global
        public static readonly string dbUrl;
        public static readonly string dbName;
        public static readonly int dbPort;
        public static readonly string dbUser;
        public static readonly string dbPassword;
        #endregion        
        
        static GlobalVariables()
        {
            // Configuração WebDriver
            browser = JsonConfigReader.GetAppSetting("BROWSERCHROME");
            isRemote = bool.Parse(JsonConfigReader.GetAppSetting("ISREMOTE"));
            headless = bool.Parse(JsonConfigReader.GetAppSetting("HEADLESS"));
            url = JsonConfigReader.GetAppSetting("DEFAULT_APPLICATION_URL_HUB");
            defaultTimeOut = JsonConfigReader.GetAppSetting("DEFAULT_TIMEOUT_IN_SECONDS");
            seleniumHub = JsonConfigReader.GetAppSetting("SELENIUM_HUB");
            driverQuit = true;

            // Configuração de relatórios
            reportName = JsonConfigReader.GetAppSetting("REPORT_NAME");
            screenshotStep = JsonConfigReader.GetAppSetting("GET_SCREENSHOT_FOR_EACH_STEP");

            // Configuração do banco de dados
            dbUrl = JsonConfigReader.GetAppSetting("DB_URL");
            dbName = JsonConfigReader.GetAppSetting("DB_NAME");
            dbPort = int.Parse(JsonConfigReader.GetAppSetting("DB_PORT"));
            dbUser = JsonConfigReader.GetAppSetting("DB_USER");
            dbPassword = JsonConfigReader.GetAppSetting("DB_PASSWORD");
        }
    }    
}

