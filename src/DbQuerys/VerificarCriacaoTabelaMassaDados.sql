SELECT 
    CASE 
        WHEN EXISTS (
            SELECT 1 
            FROM information_schema.tables 
            WHERE table_schema = DATABASE() 
            AND table_name = 'massa_dados_criacao_projetos'
        ) 
        THEN 'OK'        
    END AS table_status;