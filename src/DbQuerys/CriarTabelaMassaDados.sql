CREATE TABLE massa_dados_criacao_projetos (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nomeProjeto VARCHAR(255) NOT NULL,
    opcaoEstado VARCHAR(255) NOT NULL,
    opcaoViabilidade VARCHAR(255) NOT NULL,
    descricaoProjeto TEXT NOT NULL    
);
