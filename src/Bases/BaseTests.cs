using src.Helpers;
using NUnit.Framework;

namespace src.Basetests
{   
    public class TestsBase
    {
        [OneTimeSetUp]
        public virtual void OneTimeSetup()
        {
            ExtentReportHelpers.CreateReport();
        }

        [SetUp]
        public virtual void Setup()
        {
            
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
            ExtentReportHelpers.GenerateReport();
        }
    }
}



