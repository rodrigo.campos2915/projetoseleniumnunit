using System.Diagnostics;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using src.Helpers;
using src.Core;
using src.Pages;
using SeleniumExtras.WaitHelpers;



namespace src.Bases;
public class PageBases
{   
    
    #region Objetos | Parametros
    protected OpenQA.Selenium.Support.UI.WebDriverWait? wait { get; private set; }
    public IJavaScriptExecutor JavaScript { get; }
    protected IWebDriver driver;    
    #endregion

    #region Construtor
    public PageBases()
    {   
        driver = DriverHelper.INSTANCE;      
        wait = new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(Convert.ToInt32(GlobalVariables.defaultTimeOut)));
        JavaScript = (IJavaScriptExecutor)driver;
    }
    #endregion
    
    #region Métodos
    
    protected void NavegatePage(string url){
        driver.Navigate().GoToUrl(url);        
    }

    protected void RefreshPage(){
        driver.Navigate().Refresh();        
    }

    protected string AbsolutePath(){    

        string absolutePath = new Uri(driver.Url).AbsolutePath;
        return absolutePath;
    }

    protected IWebElement WaitForElement(By locator)
        {
            wait?.Until(ExpectedConditions.ElementExists(locator));
            IWebElement element = driver.FindElement(locator);
            wait?.Until(ExpectedConditions.ElementToBeClickable(element));
            return element;
        } 

    protected void SendKeys(By locator, string text)
    {
        WaitForElement(locator).SendKeys(text);
        ExtentReportHelpers.AddTestInfo(3, "PARAMETER: " + text); 
    }

    protected void Click(By locator)
        {
            Stopwatch timeOut = new Stopwatch();
            timeOut.Start();

            while (timeOut.Elapsed.Seconds <= Convert.ToInt32(GlobalVariables.defaultTimeOut))
            {
                try
                {
                    WaitForElement(locator).Click();
                    timeOut.Stop();     
                    ExtentReportHelpers.AddTestInfo(3, "");               
                    return;
                }
                catch (System.Reflection.TargetInvocationException)
                {

                }
                catch (StaleElementReferenceException)
                {

                }
                catch (System.InvalidOperationException)
                {

                }
                catch (WebDriverException e)
                {
                    if (e.Message.Contains("Other element would receive the click"))
                    {
                        continue;
                    }

                    if (e.Message.Contains("Element is not clickable at point"))
                    {
                        continue;
                    }

                    throw new Exception("Erro ao clicar no elemento", e);
                }
            }

            throw new Exception("Given element isn't visible");
        }

    protected string GetText(By locator)
    {
        string text = WaitForElement(locator).Text; 
        ExtentReportHelpers.AddTestInfo(3, "RETURN: " + text);    
        return text;
    }
    
    protected string GetValue(By locator)
    {
        string text = WaitForElement(locator).GetAttribute("value");
        ExtentReportHelpers.AddTestInfo(3, "RETURN: " + text);
        return text;
    }

    protected void ClearText(By locator)
    {
        WaitForElement(locator).Clear();
    }    
    
    protected void WindowMaximize()
    {
        driver.Manage().Window.Maximize();
    }
    
    protected bool GetTitlePageBool(string text)
    {    
        bool titleExist = driver.Title.Contains(text); 
        ExtentReportHelpers.AddTestInfo(3, "RETURN: " + text);
        return titleExist;       
    }

    protected string GetTitlePageString()
    {    
        string title = driver.Title; 
        ExtentReportHelpers.AddTestInfo(3, "RETURN: " + title);
        return title;       
    }
        
    protected void SelectedOption(By locator, string option)
    {
        IWebElement selectElement = WaitForElement(locator);
        SelectElement select = new SelectElement(selectElement);
        select.SelectByText(option);
        IWebElement selectedOption = select.SelectedOption;
        ExtentReportHelpers.AddTestInfo(3, "SELECTED PARAMETER: " + selectedOption.Text);       
    }
    
    protected void SendKeysEmJavascript(By locator, string text)
        {
            IWebElement element = WaitForElement(locator);
            JavaScript.ExecuteScript("arguments[0].setAttribute('value', '" + text + "');", element);
            ExtentReportHelpers.AddTestInfo(3, "PARAMETER: " + text);
        }

    protected void ClickEmJavascript(By locator)
        {
            IWebElement element = WaitForElement(locator);
            JavaScript.ExecuteScript("arguments[0].click();", element);
            ExtentReportHelpers.AddTestInfo(3, "");
        }
    
    protected string GetAttributeText(By locator)
    {    
        string text = WaitForElement(locator).GetAttribute("innerHTML");     
        ExtentReportHelpers.AddTestInfo(3, "RETURN: " + text);
        return text;       
    }

    protected void MaximizeWindowJavascript()
        {
            var screenWidth = (long)JavaScript.ExecuteScript("return screen.width;");
            var screenHeight = (long)JavaScript.ExecuteScript("return screen.height;");
            JavaScript.ExecuteScript($"window.resizeTo({screenWidth}, {screenHeight});");
        }

    #endregion
}
