using src.Pages;
using src.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace src.Flows
{
    public class AlterarContaUsuarioFlows
    {
        #region Construtor
        AlterarContaUsuarioPage alterarContaUsuario;

        public AlterarContaUsuarioFlows()
        {
            alterarContaUsuario = new AlterarContaUsuarioPage();
        }
        #endregion
        
        #region Métodos
        public void CadastrarConta(string senhaAtual, string senhaNova,string confirmaNovaSenha)
        {             
            alterarContaUsuario.PreencherSenhaAtual(senhaAtual);
            alterarContaUsuario.PreencherNovaSenha(senhaNova);
            alterarContaUsuario.PreencherConfirmarNovaSenha(confirmaNovaSenha);
            alterarContaUsuario.ClicarEmAtualizarUsuario();           
        }   
        #endregion    
    }
}