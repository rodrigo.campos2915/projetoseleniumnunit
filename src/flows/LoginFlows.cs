using src.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace src.Flows
{
    public class LoginFlows
    {
        #region Construtor
        LoginPage loginPage;

        public LoginFlows()
        {
            loginPage = new LoginPage();
        }
        #endregion
        
        #region Métodos 
        public void EfetuarLoginEmJavaScript(string username, string password)
        {                        
            loginPage.MaximizarJanelaJavaScript();      
            loginPage.PreencherUsuariosJavaScript(username);
            loginPage.ClicarEmEntrarJavaScript();
            loginPage.PreencherSenhaJavaScript(password);
            loginPage.ClicarEmEntrarJavaScript();
        }

        public void EfetuarLogin(string username, string password)
        {             
            loginPage.PreencherUsuario(username);
            loginPage.ClicarEmEntrar();
            loginPage.PreencherSenha(password);
            loginPage.ClicarEmEntrar();
        }
        #endregion
    }
}