using src.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace src.Flows
{
    public class MenuFlows
    {
        #region Construtor
        MenuPage menuPage;

        public MenuFlows()
        {
            menuPage = new MenuPage();
        }
        #endregion

        #region Métodos 
        public void NavegarParaAreaCriacaoProjeto()
        {                 
            menuPage.ClicarEmGerenciar();
            menuPage.ClicarEmAbaGerenciarProjeto();
            menuPage.ClicarEmCriarNovoProjeto();            
        }

        public void NavegarParaAreaMinhaConta()
        {            
            menuPage.ClicarEmAdministrator();          
        }

        public void NavegarParaAbaPreferencias()
        {   
            menuPage.ClicarEmAdministrator();           
            menuPage.ClicarEmAdministradorSair();  
            menuPage.ClicarEmAbaPreferencias();            
        }

        public void NavegarParaAbaGerenciarColunas()
        {   
            menuPage.ClicarEmAdministrator();                     
            menuPage.ClicarEmAdministradorSair();  
            menuPage.ClicarEmAbaGerenciarColunas();            
        }

        public void NavegarParaAbaPerfis()
        {    
            menuPage.ClicarEmAdministrator();                 
            menuPage.ClicarEmAdministradorSair();  
            menuPage.ClicarEmAbaPerfis();            
        }

        public void NavegarParaAbaTokenApis()
        {      
            menuPage.ClicarEmAdministrator();               
            menuPage.ClicarEmAdministradorSair();  
            menuPage.ClicarEmTokenApis();            
        }

        public void NavegarParaAreaGerenciarUsuarios()
        {          
            menuPage.ClicarEmGerenciarUsuarios();          
        }

        public void NavegarParaAreaGerenciarPerfis()
        {          
            menuPage.ClicarEmGerenciarPefis();          
        }

        public void SairDoSistema()
        {            
            menuPage.ClicarEmAdministradorSair();  
            menuPage.ClicarEmIconeSair();            
        }


        #endregion
    }
}