using src.Pages;
using src.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace src.Flows
{
    public class ConfiguracaoInicialMantisFlows
    {
        #region Construtor
        ConfiguracaoInicialMantisPage configuracaoInicialMantisPage;

        public ConfiguracaoInicialMantisFlows()
        {
            configuracaoInicialMantisPage = new ConfiguracaoInicialMantisPage();
        }
        #endregion
        
        #region Métodos
        public void FazerConfiguracaoInicial(DadosConfiguracaoInicialMantis dadosConfiguracaoInicial)
        {              
            configuracaoInicialMantisPage.SelecionarOpcaoTipoBancoDados(dadosConfiguracaoInicial.tipoBancoDados);
            configuracaoInicialMantisPage.PreencherNomeServidor(dadosConfiguracaoInicial.nomeServidor);
            configuracaoInicialMantisPage.PreencherUsuario(dadosConfiguracaoInicial.nomeUsuario);
            configuracaoInicialMantisPage.PreencherSenha(dadosConfiguracaoInicial.senhaUsuario);
            configuracaoInicialMantisPage.PreencherUsuarioAdmin(dadosConfiguracaoInicial.nomeUsuarioAdmin);
            configuracaoInicialMantisPage.PreencherSenhaAdmin(dadosConfiguracaoInicial.nomeSenhaAdmin);            
            configuracaoInicialMantisPage.ClicarEmInstalar();
        }
        #endregion
    }
}