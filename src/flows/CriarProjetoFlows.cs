using src.Pages;
using src.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace src.Flows
{
    public class CriarProjetoFlows
    {
        #region Construtor
        CriarProjetoPage criarProjetoPage;

        public CriarProjetoFlows()
        {
            criarProjetoPage = new CriarProjetoPage();
        }
        #endregion
        
        #region Métodos
        public void CriarProjeto(DadosCriacaoProjetoMantis dadosCriacaoProjeto)
        {                         
            criarProjetoPage.PreencherNomeProjeto(dadosCriacaoProjeto.nomeProjeto);
            criarProjetoPage.SelecionarOpcaoEstado(dadosCriacaoProjeto.opcaoEstado);
            criarProjetoPage.SelecionarCategoriasGlobais(dadosCriacaoProjeto.selecionarCategoriaGlobais);
            criarProjetoPage.SelecionarOpcaoViabilidade(dadosCriacaoProjeto.opcaoViabilidade);
            criarProjetoPage.PreencherDescricao(dadosCriacaoProjeto.descricao);
            criarProjetoPage.ClicarEmEntrar();
        }
        #endregion
    }
}