# Usar a imagem oficial do .NET como a imagem base
FROM mcr.microsoft.com/dotnet/sdk:8.0.101 AS build-env
WORKDIR /app

# Copiar csproj e restaurar dependências
COPY *.sln .
COPY src/*.csproj ./src/
RUN dotnet restore

# Copiar o restante dos arquivos e compilar
COPY . ./
RUN dotnet build -c Release -o out

# Criar uma imagem menor para a execução
FROM mcr.microsoft.com/dotnet/sdk:8.0.101
WORKDIR /app
COPY --from=build-env /app/out .

ENTRYPOINT ["dotnet", "ProjetoSeleniumNunit.dll"]
